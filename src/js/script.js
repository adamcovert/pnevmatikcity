$(document).ready(function() {

  svg4everybody();

  $('.s-layout__more-btn').on('click', function() {
    var button = $(this);
    var text = button.text() == 'Показать больше' ? 'Показать меньше' : 'Показать больше';
    button.text(text);
  });

  $('.s-hamburger').on('click', function () {
    $('.s-mobile-menu').addClass('s-mobile-menu--is-shown');
    $('body').addClass('mobile-menu-is-open');
  });

  $('.s-mobile-menu__close').on('click', function () {
    $('.s-mobile-menu').removeClass('s-mobile-menu--is-shown');
    $('body').removeClass('mobile-menu-is-open');
  });

  $('.s-about-us__certificates').lightGallery({
    selector: 'a',
    mode: 'lg-fade',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    thumbnail: true,
    animateThumb: false,
    showThumbByDefault: false
  });

  var promoSlider = new Swiper('.s-promo__slider', {
    effect: 'fade',
    pagination: {
      el: '.swiper-pagination',
      clickable: true
    },
  });

  var bannersSlider = new Swiper('.s-banners__slider', {
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
      dynamicBullets: true
    },
    autoplay: {
      delay: 5000,
    },
    breakpoints: {
      374: {
        slidesPerView: 1,
        spaceBetween: 10
      },
      767: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      991: {
        slidesPerView: 3,
        spaceBetween: 10
      }
    }
  });

  var productSlider = new Swiper('.s-product__slider', {
    effect: 'fade',
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
      dynamicBullets: true
    }
  });

  var partsSlider = new Swiper('.s-parts__slider', {
    slidesPerView: 3,
    spaceBetween: 30,
    breakpoints: {
      424: {
        slidesPerView: 1,
        spaceBetween: 15
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 30
      }
    }
  });
});